use std::io::BufRead;
use std::process::Command;
use clap::{ArgAction, Parser};
use procfs::process::Process;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// only print file descriptor info to stdout (rest to stderr)
    #[arg(short, long, action = ArgAction::SetTrue)]
    quiet: bool,

    /// the PID of the process to run lsof for
    #[arg(short, long)]
    pid: String,

    /// arguments to pass directly to lsof
    #[arg(last = true)]
    lsof_args: Vec<String>,
}

fn main() {
    let args = Args::parse();

    let pid = args.pid.parse::<u32>().unwrap();

    let process = Process::new(pid as i32).unwrap().task_from_tid(pid as i32);
    let child_processes = process.unwrap().children().unwrap();
    let child_processes_count = child_processes.len();

    match args.quiet {
        true => {}
        false => eprintln!("The process {pid} has {child_processes_count} children: {child_processes:?}")
    };

    let pids: Vec<u32> = [vec![pid], child_processes].concat();

    for (_i_outer, pid) in pids.iter().enumerate() {
        let output = Command::new("lsof").arg("-p").arg(format!("{pid}")).args(&args.lsof_args).output().unwrap();
        for (i_inner, line) in output.stdout.lines().enumerate() {

            // for child processes, print table header to stderr or not at all
            match i_inner {
                0 => match args.quiet {
                    true => {}
                    false => eprintln!("{}", line.unwrap())
                }
                _ => println!("{}", line.unwrap())
            }
        }
    }

}
